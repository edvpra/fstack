import XCTest

class FStackTests: XCTestCase {
    
    let loadWaitTimeout = 10.0

    override func setUp() {
        continueAfterFailure = true
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetMarketSummary() throws {
        let expectation = self.expectation(description: "Query timed out")
        var _error: Error?
        var _json: [String : Any]?
        
        YahooFinanceApi.getMarketSummary { (json: [String : Any]?, error: Error?) in
            _error = error
            _json = json
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: loadWaitTimeout) { (error: Error?) in
            XCTAssert(error == nil, error?.localizedDescription ?? "")
            XCTAssert(_error == nil, _error?.localizedDescription ?? "")
            XCTAssert(_json != nil, "No data returned")
        }
    }
}
