import UIKit

class MainViewController: UIViewController {
    
    private let kRefreshInterval: TimeInterval = 8.0
    
    private let searchBar = UISearchBar()
    private let dataModel = ListOfMarketsDataModel()
    private var refreshTimer: Timer?
    
    @IBOutlet weak var tableView: UITableView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Market Summary"
        
        self.navigationItem.titleView = searchBar
        self.searchBar.delegate = self
        self.searchBar.autocapitalizationType = .none
        
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.reloadData()
    }
        
    @objc func reloadData() {
        self.dataModel.loadData { [weak self] in
            DispatchQueue.main.async {
                self?.tableView?.reloadData()
            }
        }
    }
    
    private func startRefreshTimer() {
        self.refreshTimer = Timer.scheduledTimer(timeInterval: kRefreshInterval,
                                                 target: self,
                                                 selector: #selector(reloadData),
                                                 userInfo: nil,
                                                 repeats: true)
    }
    
    private func stopRefreshTimer() {
        self.refreshTimer?.invalidate()
        self.refreshTimer = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startRefreshTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopRefreshTimer()
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataModel.filteredItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = "marketListCellReuseIdentifier"
        var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: reuseIdentifier)
        }
        
        let marketItem = self.dataModel.filteredItems[indexPath.row]
        cell.textLabel?.text = marketItem.name
        cell.detailTextLabel?.text = "\(marketItem.regularMarketPrice) (\(marketItem.regularMarketChangeFmt))"
            
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let marketItem = self.dataModel.filteredItems[indexPath.row]
        let detailViewController = DetailViewController()
        detailViewController.symbol = marketItem.symbol
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}

extension MainViewController: UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.dataModel.searchString = searchBar.text
        self.tableView?.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.dataModel.searchString = searchBar.text
        self.tableView?.reloadData()
    }
}
