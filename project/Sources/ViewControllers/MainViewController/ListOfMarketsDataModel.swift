import Foundation

class ListOfMarketsDataModel {
    var searchString: String? {
        didSet {
            filterItems(string: searchString)
        }
    }
    
    private var allItems: [MarketEntity] = []
    var filteredItems: [MarketEntity] = []

    private func filterItems(string: String?) {
        guard let string = string, !string.isEmpty else {
            self.filteredItems = self.allItems
            return
        }
        
        // filter items
        self.filteredItems = self.allItems.filter({ (item: MarketEntity) -> Bool in
            return item.name.lowercased().contains(string.lowercased())
        })
    }
    
    func loadData(completion: (() -> Void)?) {
        YahooFinanceApi.getMarketSummary { [weak self] (json: [String : Any]?, error: Error?) in
            if let error = error {
                debugPrint(error.localizedDescription)
                return
            }
            
            guard let json = json else {
                debugPrint("No data received")
                return
            }
                        
            if let error = json["error"] {
                debugPrint("error: \(error)")
            }
            
            guard let response = json["marketSummaryResponse"] as? [String : Any] else {
                debugPrint("Invalid data received: \(json)")
                return
            }

            guard let result = response["result"] as? [[String : Any]] else {
                debugPrint("Invalid data received: \(json)")
                return
            }
            
            var items: [MarketEntity] = []
            
            for item in result {
                let marketEntity = MarketEntity(with: item)
                items.append(marketEntity)
            }
            
            self?.allItems = items
            self?.filterItems(string: self?.searchString)
            completion?()
        }
    }
}
