import UIKit

class DetailViewController: UIViewController {
    
    let dataModel = DetailViewDataModel()
    var symbol: String?
    
    // response will be parsed to flat array of (String, String)
    @IBOutlet weak var tableView: UITableView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Stock Summary"
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        
        self.dataModel.loadData(symbol: self.symbol) { [weak self] in
            DispatchQueue.main.async {
                self?.tableView?.reloadData()
            }
        }
    }
}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = "marketDetailsCellReuseIdentifier"
        var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: reuseIdentifier)
        }
        
        let item = self.dataModel.items[indexPath.row]
        cell.textLabel?.text = item.0
        cell.detailTextLabel?.text = item.1
            
        return cell;
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}
