import Foundation

class DetailViewDataModel {

    // response format is not well documented so in this
    // demo we just display content as flat array (includeing nested structures)

    var items: [(String, String)] = []

    private func mapToArray(map: [String : Any]) -> [(String, String)] {
        var arr: [(String, String)] = []
        
        for key in map.keys {
            if let stringValue = map[key] as? String {
                arr.append((key, stringValue))
            }
            else if let nestedMap = map[key] as? [String : Any] {
                let arr2 = mapToArray(map: nestedMap)
                arr.append(contentsOf: arr2)
            }
        }

        return arr
    }

    func loadData(symbol: String?, completion: (() -> Void)?) {
        guard let symbol = symbol?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion?()
            return
        }
                
        YahooFinanceApi.getStockSummary(symbol: symbol) { [weak self] (json: [String : Any]?, error: Error?) in
            if let error = error {
                debugPrint(error.localizedDescription)
                return
            }
            
            guard let json = json else {
                debugPrint("No data received")
                return
            }
            
            if let items = self?.mapToArray(map: json) {
                self?.items = items
            }
            
            completion?()
        }
    }
}
