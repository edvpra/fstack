import Foundation

struct MarketEntity {
    var data: [String : Any]?
    
    init(with data: [String : Any]) {
        self.data = data
    }
    
    var symbol: String {
        return data?["symbol"] as? String ?? ""
    }
    
    var fullExchangeName: String {
        return data?["fullExchangeName"] as? String ?? ""
    }
    
    var shortName: String {
        return data?["shortName"] as? String ?? ""
    }
    
    var regularMarketPrice: String {
        guard let change = data?["regularMarketPrice"] as? [String : Any] else {
            return ""
        }
        
        return change["fmt"] as? String ?? ""
    }
    
    var regularMarketChangeFmt: String {
        guard let change = data?["regularMarketChange"] as? [String : Any] else {
            return ""
        }
        
        return change["fmt"] as? String ?? ""
    }

    var name: String {
        return "\(fullExchangeName) \(shortName) (\(symbol))"
    }
}
