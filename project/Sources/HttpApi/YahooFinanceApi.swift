import Foundation

class YahooFinanceApi {
    
    static let commonHeaders = ["Accept" : "application/json",
                                "x-rapidapi-host" : "apidojo-yahoo-finance-v1.p.rapidapi.com",
                                "x-rapidapi-key" : "0ac11d7c77mshd895343ecfa9478p123a60jsn7c9fa9555a08"]
    
    class func getMarketSummary(completion: (([String: Any]?, Error?) -> Void)?) {
        debugPrint(#function)
        
        let url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/get-summary?region=US"
        var headers = commonHeaders
        headers["useQueryString"] = "true"
        
        HttpNetworkManager.requestWithUrl(url, method: .GET, headers: headers) { (data, response, error) in
            
            guard let data = data, error == nil else {
                completion?(nil, error)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                completion?(json, nil)
            }
            catch {
                completion?(nil, error)
            }
        }
    }

    class func getStockSummary(symbol: String, completion: (([String: Any]?, Error?) -> Void)?) {
        debugPrint(#function)
        
        let url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-summary?symbol=\(symbol)&region=US"
                
        HttpNetworkManager.requestWithUrl(url, method: .GET, headers: commonHeaders) { (data, response, error) in
            guard let data = data, error == nil else {
                completion?(nil, error)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                completion?(json, nil)
            }
            catch {
                completion?(nil, error)
            }
        }
    }
}
