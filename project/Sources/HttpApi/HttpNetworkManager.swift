import Foundation

class HttpNetworkManager {
    
    enum HttpMethod: String {
        case GET = "GET"
        case POST = "POST"
        case PUT = "PUT"
        case DELETE = "DELETE"
    }
    
    class func requestWithUrl(_ url: String, method: HttpMethod = .GET, headers: [String : String]? = nil, data: Data? = nil, completion: ((Data?, URLResponse?, Error?) -> Void)?) {
        debugPrint(#function)
        
        guard let requestUrl = URL(string: url) else {
            debugPrint("Invalid url: \(url)")
            completion?(nil, nil, nil)
            return
        }

        var request = URLRequest(url: requestUrl)
        request.httpMethod = method.rawValue
        request.httpBody = data
        
        if let headers = headers {
            for key in headers.keys {
                request.setValue(headers[key], forHTTPHeaderField: key)
            }
        }

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            completion?(data, response, error)
        }
        
        task.resume()
    }
}
